const HtmlWebpackPlugin = require('html-webpack-plugin');

module.exports = {
	devtool: 'none',
	entry: {
		main: './src/js/index.js',
		polyfills: './src/js/polyfills.js',
		vendor: './src/js/vendor/vendor.js',
	},
	module: {
		rules: [
			{ test: /\.ts$/, use: 'ts-loader' },
			{
				test: /\.(svg|png|jpg|gif)$/,
				use: {
					loader: 'file-loader',
					options: {
						name: '[name].[hash].[ext]',
						outputPath: 'img',
					},
				},
			},
		],
	},
	plugins: [
		new HtmlWebpackPlugin({
			template: './src/index.html',
		}),
	],
};
