!(function(e) {
  var n = {};
  function o(t) {
    if (n[t]) return n[t].exports;
    var r = (n[t] = { i: t, l: !1, exports: {} });
    return e[t].call(r.exports, r, r.exports, o), (r.l = !0), r.exports;
  }
  (o.m = e),
    (o.c = n),
    (o.d = function(e, n, t) {
      o.o(e, n) || Object.defineProperty(e, n, { enumerable: !0, get: t });
    }),
    (o.r = function(e) {
      'undefined' != typeof Symbol &&
        Symbol.toStringTag &&
        Object.defineProperty(e, Symbol.toStringTag, { value: 'Module' }),
        Object.defineProperty(e, '__esModule', { value: !0 });
    }),
    (o.t = function(e, n) {
      if ((1 & n && (e = o(e)), 8 & n)) return e;
      if (4 & n && 'object' == typeof e && e && e.__esModule) return e;
      var t = Object.create(null);
      if (
        (o.r(t),
        Object.defineProperty(t, 'default', { enumerable: !0, value: e }),
        2 & n && 'string' != typeof e)
      )
        for (var r in e)
          o.d(
            t,
            r,
            (n => {
              return e[n];
            }).bind(null, r)
          );
      return t;
    }),
    (o.n = function(e) {
      var n =
        e && e.__esModule
          ? function() {
              return e.default;
            }
          : function() {
              return e;
            };
      return o.d(n, 'a', n), n;
    }),
    (o.o = function(e, n) {
      return Object.prototype.hasOwnProperty.call(e, n);
    }),
    (o.p = ''),
    o((o.s = 3));
})([
  function(e, n, o) {
    (function(n) {
      const t = o(5);
      const r = o(2).execSync;
      const s = o(6);
      const c = __dirname;
      let i = 'cd';
      'darwin' === s.platform() && (i = 'echo $PWD');
      const a = r(i)
        .toString()
        .replace('\n', '')
        .replace('\r', '');
      e.exports = {
        baseExportPath: a,
        baseImportPath: c,
        tsconfig: {
          compilerOptions: {
            target: 'es2015',
            module: 'ESNext',
            lib: ['es2018', 'dom'],
            declarationMap: !1,
            sourceMap: !0,
            importHelpers: !0,
            strict: !0,
            moduleResolution: 'node',
            typeRoots: ['node_modules/@types'],
            esModuleInterop: !0,
            experimentalDecorators: !0,
            emitDecoratorMetadata: !0
          }
        },
        tslint: {
          extends: 'tslint:recommended',
          rules: {
            'array-type': !1,
            'arrow-parens': !1,
            deprecation: { severity: 'warn' },
            'component-class-suffix': !0,
            'contextual-lifecycle': !0,
            'directive-class-suffix': !0,
            'directive-selector': [!0, 'attribute', 'app', 'camelCase'],
            'component-selector': [!0, 'element', 'app', 'kebab-case'],
            'import-blacklist': [!0, 'rxjs/Rx'],
            'interface-name': !1,
            'max-classes-per-file': !1,
            'max-line-length': [!0, 140],
            'member-access': !1,
            'member-ordering': [
              !0,
              { order: ['static-field', 'instance-field', 'static-method', 'instance-method'] }
            ],
            'no-consecutive-blank-lines': !1,
            'no-console': [!0, 'debug', 'info', 'time', 'timeEnd', 'trace'],
            'no-empty': !1,
            'no-inferrable-types': [!0, 'ignore-params'],
            'no-non-null-assertion': !0,
            'no-redundant-jsdoc': !0,
            'no-switch-case-fall-through': !0,
            'no-use-before-declare': !0,
            'no-var-requires': !1,
            'object-literal-key-quotes': [!0, 'as-needed'],
            'object-literal-sort-keys': !1,
            'ordered-imports': !1,
            quotemark: [!0, 'single'],
            'trailing-comma': !1,
            'no-conflicting-lifecycle': !0,
            'no-host-metadata-property': !0,
            'no-input-rename': !0,
            'no-inputs-metadata-property': !0,
            'no-output-native': !0,
            'no-output-on-prefix': !0,
            'no-output-rename': !0,
            'no-outputs-metadata-property': !0,
            'template-banana-in-box': !0,
            'template-no-negated-async': !0,
            'use-lifecycle-interface': !0,
            'use-pipe-transform-interface': !0
          },
          rulesDirectory: []
        },
        eslint: {
          env: { browser: !0, es6: !0 },
          extends: 'eslint:recommended',
          globals: { Atomics: 'readonly', SharedArrayBuffer: 'readonly' },
          parserOptions: { ecmaVersion: 2018, sourceType: 'module' },
          rules: {
            'no-console': 'off',
            'arrow-spacing': 'warn',
            camelcase: 'warn',
            'constructor-super': 'error',
            'max-len': ['warn', { code: 100 }],
            'one-var': ['warn', 'never'],
            'require-await': 'warn',
            'prefer-arrow-callback': 'warn',
            'prefer-const': 'warn',
            quotes: ['warn', 'single'],
            'valid-typeof': 'error',
            'no-unused-vars': 'warn',
            'no-unused-expressions': 'warn'
          }
        }
      };
    }.call(this, '/'));
  },
  function(e, n) {
    e.exports = require('fs');
  },
  function(e, n) {
    e.exports = require('child_process');
  },
  function(e, n, o) {
    const t = o(4);
    const r = o(7);
    const s = o(8);
    const c = o(9);
    const i = o(10);
    const a = o(11);
    const l = o(12);
    try {
      t.generate(),
        console.log('Generated .gitignore File \n\n'),
        r.init(),
        console.log('Node-Projekt initialised \n\n'),
        s.modifyPackageJSON(),
        console.log('Package.json updated \n\n'),
        c.configurate(),
        console.log('Webpack is now configured \n\n'),
        i.configurate(),
        console.log('Created tsconfig.json and tslint.json \n\n'),
        a.configurate(),
        console.log('Created eslintrc.json \n\n'),
        l.generate(),
        console.log('Projekt-Folderstructure generated \n\n'),
        console.log(
          '=================== Thank You for using this Workspace-Generator ===================== \n\n'
        );
    } catch (e) {
      console.error(e);
    }
  },
  function(e, n, o) {
    const t = o(1);
    const r = o(0).baseExportPath;
    const s = ['node_modules', 'build', 'scripts'];
    e.exports.generate = function() {
      const e = s.join('\n');
      t.writeFileSync(r + '/.gitignore', e);
    };
  },
  function(e, n) {
    e.exports = require('path');
  },
  function(e, n) {
    e.exports = require('os');
  },
  function(e, n, o) {
    const t = o(2).execSync;
    const r = [
      'clean-webpack-plugin',
      'css-loader',
      'file-loader',
      'html-loader',
      'html-webpack-plugin',
      'mini-css-extract-plugin',
      'optimize-css-assets-webpack-plugin',
      'sass sass-loader',
      'style-loader',
      'webpack',
      'webpack-cli',
      'webpack-dev-server',
      'webpack-merge',
      'ts-loader',
      'tslint',
      'typescript',
      'eslint'
    ];
    e.exports.init = function() {
      t('npm init', { stdio: [0, 1, 2] }),
        console.log('Package.json created'),
        t('npm i', { stdio: [0, 1, 2] }),
        console.log('Basic dependencies installed'),
        t('npm i --save-dev ' + r.join(' '), { stdio: [0, 1, 2] }),
        console.log('Other dependencies installed');
    };
  },
  function(e, n, o) {
    const t = o(1);
    const r = o(0).baseExportPath;
    e.exports.modifyPackageJSON = function() {
      var e = JSON.parse(t.readFileSync(r + '/package.json').toString());
      console.log('found package.json'),
        (e.scripts.serve = 'webpack-dev-server --open --config webpack.dev.js'),
        (e.scripts.build = 'webpack --config webpack.prod.js'),
        delete e.scripts.test,
        console.log('added new scripts'),
        (e.private = !0),
        delete e.license,
        delete e.homepage,
        console.log('changed licensing'),
        t.writeFileSync(r + '/package.json', JSON.stringify(e)),
        console.log('saved modifications');
    };
  },
  function(e, n, o) {
    const t = o(1);
    const r = o(0);
    const s = { config: 'webpack.config.js', prod: 'webpack.prod.js', dev: 'webpack.dev.js' };
    e.exports.configurate = function() {
      console.log('generating Webpack configuration \n');
      for (const e in s)
        if (Object.hasOwnProperty.call(s, e)) {
          const n = s[e];
          const o = t.readFileSync(r.baseImportPath + '/presets/wpConfig/' + n);
          t.writeFileSync(r.baseExportPath + '/webpack.' + e + '.js', o),
            console.log('Successfully generated webpack.' + e + '.js');
        } else console.error('Couldn\'t find webpack.' + e + '.js configuration');
    };
  },
  function(e, n, o) {
    const t = o(1);
    const r = o(0);
    e.exports.configurate = function() {
      t.writeFileSync(r.baseExportPath + '/tsconfig.unused.json', JSON.stringify(r.tsconfig)),
        t.writeFileSync(r.baseExportPath + '/tslint.json', JSON.stringify(r.tslint));
    };
  },
  function(e, n, o) {
    const t = o(1);
    const r = o(0);
    e.exports.configurate = function() {
      t.writeFileSync(r.baseExportPath + '/eslintrc.json', JSON.stringify(r.eslint));
    };
  },
  function(e, n, o) {
    const t = o(1);
    const r = o(0).baseImportPath;
    const s = o(0).baseExportPath;
    const c = { flag: 'w' };
    const i = (e, n) => {
      console.log('creating File from: ' + r + e + '\n at: ' + s + n);
      const o = t.readFileSync(r + e).toString();
      t.writeFileSync(s + n, o, c);
    };
    const a = e => {
      t.existsSync(s + e) || t.mkdirSync(s + e);
    };
    e.exports.generate = function() {
      a('/src'),
        a('/src/styles'),
        a('/src/js'),
        a('/src/js/vendor'),
        i('/presets/preset.html', '/src/index.html'),
        i('/presets/preset.scss', '/src/styles/index.scss'),
        i('/presets/vendor/base.css', '/src/styles/base.css'),
        i('/presets/preset.js', '/src/js/index.js'),
        i('/presets/vendor/base.js', '/src/js/vendor/base.js'),
        i('/presets/polyfills.js', '/src/js/polyfills.js'),
        i('/presets/vendor/vendor.js', '/src/js/vendor/vendor.js');
    };
  }
]);
