import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

// insert imports of new components
import { ButtonComponent } from './button/button.component';

// insert imports of new route example following below:
// const routes: Routes = [{ path: 'array', component: ArrayCalcComponent }];
const routes: Routes = [{ path: 'button', component: ButtonComponent }];

@NgModule({
  // import in here, too
  imports: [RouterModule.forRoot(routes)],
  exports: [RouterModule],
})
export class AppRoutingModule {}
